package com.bootcampjava.event;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class EventApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Test
    public void main() {
        EventApplication.main(new String[] {});
    }

}

package com.bootcamp.java.fin.service.mapper;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import com.bootcamp.java.fin.domain.Solicitud;
import com.bootcamp.java.fin.web.model.SolicitudModel;

@Mapper(componentModel = "spring")
public interface SolicitudMapper {
		
	Solicitud solicitudModelToSolicitud (SolicitudModel model);	 
	SolicitudModel solicitudToSolicitudModel (SolicitudModel solicitud);	 
	List<SolicitudModel> solicitudToSolicitudModels (List<Solicitud> domain);	 
	 void update(@MappingTarget Solicitud entity, SolicitudModel updateEntity);
	SolicitudModel solicitudToSolicitudModel(Solicitud solicitud);

}

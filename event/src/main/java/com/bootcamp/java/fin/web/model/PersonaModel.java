package com.bootcamp.java.fin.web.model;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PersonaModel {
	
@JsonProperty("PersonaId")	
	
	@Builder.Default
	private Long id =null;
@Builder.Default
@NotBlank(message="Nombre no puede ser vacio")
	private String nombres = null;
@Builder.Default
	private String apellidos= null;
@Builder.Default
	private String dni= null;
@Builder.Default
	private String correo= null;
	
}

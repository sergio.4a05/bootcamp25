package com.bootcamp.java.fin.service;


import java.util.List;

import com.bootcamp.java.fin.web.model.SolicitudModel;

public interface ISolicitudService {	
	List<SolicitudModel> findAll() throws Exception;
	SolicitudModel findById(Long id) throws Exception;
	List<SolicitudModel> findByNameAndDescription(String name, String description) throws Exception;
	SolicitudModel create(SolicitudModel eventModel) throws Exception;
	void update(Long id, SolicitudModel eventModel) throws Exception;
	void deleteById(Long id) throws Exception;
}

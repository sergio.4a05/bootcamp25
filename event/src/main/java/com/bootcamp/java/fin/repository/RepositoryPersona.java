package com.bootcamp.java.fin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bootcamp.java.fin.domain.Persona;
public interface RepositoryPersona extends JpaRepository <Persona, Long> {

}

package com.bootcamp.java.fin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.fin.domain.Solicitud;
import com.bootcamp.java.fin.web.model.SolicitudModel;
public interface RepositorySolicitud extends JpaRepository <Solicitud, Long> {
	Solicitud save(SolicitudModel solicitudToSolicitudModel);
}
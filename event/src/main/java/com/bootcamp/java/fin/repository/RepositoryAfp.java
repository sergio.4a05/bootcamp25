package com.bootcamp.java.fin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.fin.domain.Afp;
public interface RepositoryAfp extends JpaRepository <Afp, Long> {
}

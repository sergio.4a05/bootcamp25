package com.bootcamp.java.fin.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder 
@NoArgsConstructor
@AllArgsConstructor
//Creando la entidad AFP
public class Afp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String afp;
}

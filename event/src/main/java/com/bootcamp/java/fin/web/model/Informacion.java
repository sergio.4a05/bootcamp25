package com.bootcamp.java.fin.web.model;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Informacion {
	
	@JsonProperty("InfoId")
	
	@Builder.Default
	private Long id = null;
	  @NotBlank(message="DNI no puede ser vacio")
	@Builder.Default
	private String dni= null;
	
	@Builder.Default
	private Double monto_dis= null;
	
	@Builder.Default
	private String fecha_retiro= null;
}




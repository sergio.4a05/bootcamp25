package com.bootcamp.java.fin.web.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AfpModel {

	@JsonProperty("AfpId")
	@Builder.Default
	private Long id = null;
	  @NotBlank(message="afp no puede ser vacio")
	@Builder.Default
	private String afp = null;
	
}

package com.bootcamp.java.fin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.fin.domain.Informacion;
public interface RepositoryInformacion extends JpaRepository <Informacion, Long> {

}
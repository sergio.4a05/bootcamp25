package com.bootcamp.java.fin.web;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.java.fin.service.ISolicitudService;
import com.bootcamp.java.fin.web.model.SolicitudModel;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/solicitud")
@Slf4j
public class SolicitudController {
	
	private final ISolicitudService solicitudService;
	
	@GetMapping()
	@Operation(summary = "Get list of solicitud")
	public ResponseEntity<Object> getAll() throws Exception{
		List<SolicitudModel> response = solicitudService.findAll();
		log.info("getAll" + "OK");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Get list of solicitud by id")
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) throws Exception{
		SolicitudModel response = solicitudService.findById(id);
		log.info("getById" + "OK");
		log.debug(id.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping
	@Operation(summary = "Create solicitud")
	public ResponseEntity<Object> create(@RequestBody SolicitudModel eventModel) throws Exception{
		SolicitudModel response = solicitudService.create(eventModel);
		log.info("Crear" + "OK");
		log.debug(eventModel.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@PutMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Update solicitud by id")
	public void update(@PathVariable("id") Long id, 
					   @RequestBody SolicitudModel eventModel) throws Exception {
		solicitudService.update(id, eventModel);	
		log.info("Actualizando" + "OK");
		log.debug(id.toString()+ "/" + eventModel.toString());
	}
	
	@DeleteMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Delete solicitud by id")
	public void deleteById(@PathVariable("id") Long id)  throws Exception {
		solicitudService.deleteById(id);	
		log.info("EliminandoById" + "OK");
		log.debug(id.toString());
	}
	
	
}


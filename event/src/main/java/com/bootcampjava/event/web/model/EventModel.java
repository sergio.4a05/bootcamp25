package com.bootcampjava.event.web.model;

import java.sql.Timestamp;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventModel {

	@JsonProperty("eventId")
	private Long id;
	
    @NotBlank(message="Nombre no puede ser vacio")
    private String name;
	
    @NotBlank(message="Descripcion no puede ser vacio")
    private String description;
	
	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    @NotNull(message="Fecha del evento no puede ser vacio")
	@Future
	private Timestamp dateEvent;
		
}
